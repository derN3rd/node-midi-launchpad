import easymidi from "easymidi";
import drawVerticalMatrix from "./utils/drawVerticalMatrix";
import getMatrixFromText from "./utils/getMatrixFromText";
import resetLaunchpad from "./utils/resetLaunchpad";
import _setPad from "./utils/setPad";

// all data about launchpad midi from: http://leemans.ch/latex/doc_launchpad-programmers-reference.pdf

const allInputs = easymidi.getInputs();
const launchpadMidiInputName = allInputs.find((name) =>
  name.includes("Launchpad")
);
const allOutputs = easymidi.getOutputs();
const launchpadMidiOutputName = allOutputs.find((name) =>
  name.includes("Launchpad")
);

if (!launchpadMidiInputName || !launchpadMidiOutputName) {
  console.error(
    "Couldn't find Launchpad MIDI device. Found only\nInputs:",
    allInputs.join(", "),
    "\nOutputs:",
    allOutputs.join(", ")
  );
  process.exit(1);
}
const input = new easymidi.Input(launchpadMidiInputName);
const output = new easymidi.Output(launchpadMidiOutputName);

const setPad = _setPad(output);

resetLaunchpad(output);

const verticalMatrix1 = getMatrixFromText("Ich ");
const heart = [
  0b00001110,
  0b00010001,
  0b00100001,
  0b01000010,
  0b01000010,
  0b00100001,
  0b00010001,
  0b00001110,
  0,
];
const verticalMatrix3 = getMatrixFromText(" dich!  ");

const verticalMatrix = [...verticalMatrix1, ...heart, ...verticalMatrix3];

let counter = 0;
const interval = setInterval(() => {
  drawVerticalMatrix(
    output,
    [...verticalMatrix, 0, ...verticalMatrix].slice(counter, counter + 8),
    {
      color: {
        green: 0,
        red: 2,
      },
    }
  );

  if (counter === verticalMatrix.length) {
    counter = 0;
  } else {
    counter++;
  }
}, 100);

process.on("SIGINT", function () {
  try {
    clearInterval(interval);
    input.close();
    resetLaunchpad(output);
    output.close();
  } catch (e) {}
  process.kill(0);
});
