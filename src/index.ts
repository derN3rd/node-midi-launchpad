import easymidi from "easymidi";
import resetLaunchpad from "./utils/resetLaunchpad";
import _setPad from "./utils/setPad";

// all data about launchpad midi from: http://leemans.ch/latex/doc_launchpad-programmers-reference.pdf

const allInputs = easymidi.getInputs();
const launchpadMidiInputName = allInputs.find((name) =>
  name.includes("Launchpad")
);
const allOutputs = easymidi.getOutputs();
const launchpadMidiOutputName = allOutputs.find((name) =>
  name.includes("Launchpad")
);

if (!launchpadMidiInputName || !launchpadMidiOutputName) {
  console.error(
    "Couldn't find Launchpad MIDI device. Found only\nInputs:",
    allInputs.join(", "),
    "\nOutputs:",
    allOutputs.join(", ")
  );
  process.exit(1);
}
const input = new easymidi.Input(launchpadMidiInputName);
const output = new easymidi.Output(launchpadMidiOutputName);

const setPad = _setPad(output);

resetLaunchpad(output);

input.on("noteon", function (msg) {
  if (msg.velocity === 0) return;
  console.log(msg);
});

process.on("SIGINT", function () {
  try {
    input.close();
    resetLaunchpad(output);
    output.close();
  } catch (e) {}
  process.kill(0);
});
