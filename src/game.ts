import easymidi from "easymidi";
import drawMatrix, { Matrix } from "./utils/drawMatrix";
import resetLaunchpad from "./utils/resetLaunchpad";
import _setPad from "./utils/setPad";

// all data about launchpad midi from: http://leemans.ch/latex/doc_launchpad-programmers-reference.pdf

const allInputs = easymidi.getInputs();
const launchpadMidiInputName = allInputs.find((name) =>
  name.includes("Launchpad")
);
const allOutputs = easymidi.getOutputs();
const launchpadMidiOutputName = allOutputs.find((name) =>
  name.includes("Launchpad")
);

if (!launchpadMidiInputName || !launchpadMidiOutputName) {
  console.error(
    "Couldn't find Launchpad MIDI device. Found only\nInputs:",
    allInputs.join(", "),
    "\nOutputs:",
    allOutputs.join(", ")
  );
  process.exit(1);
}
const input = new easymidi.Input(launchpadMidiInputName);
const output = new easymidi.Output(launchpadMidiOutputName);

const setPad = _setPad(output);

resetLaunchpad(output);

const matrix = [
  [0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0],
] as Matrix;

const getMatrixAddress = (decimalAddress: number) => {
  const hexAddress = decimalAddress.toString(16);
  const zeroFilledHexAddress = "00".substr(hexAddress.length) + hexAddress;

  return zeroFilledHexAddress.split("").map(Number);
};
const checkIfValidMatrixAddress = (x: number, y: number) =>
  x >= 0 && x < 8 && y >= 0 && y < 8;

const generateRandomMatrix = () => {
  matrix.forEach((row, rowIndex) => {
    matrix[rowIndex] = row.map((column) => Math.round(Math.random())) as [
      number,
      number,
      number,
      number,
      number,
      number,
      number,
      number
    ];
  });
};

const flipNeighbors = (x: number, y: number) => {
  matrix[y][x] ^= 1;
  if (x > 0) {
    matrix[y][x - 1] ^= 1;
  }
  if (x < 7) {
    matrix[y][x + 1] ^= 1;
  }
  if (y > 0) {
    matrix[y - 1][x] ^= 1;
  }
  if (y < 7) {
    matrix[y + 1][x] ^= 1;
  }
};

generateRandomMatrix();
drawMatrix(output, matrix, {
  color: {
    green: 3,
    red: 1,
  },
});

let greenBrightness = 1;
let redBrightness = 1;

input.on("noteon", (msg) => {
  if (msg.velocity === 0) return;

  const [y, x] = getMatrixAddress(msg.note);
  const draw = () => {
    drawMatrix(output, matrix, {
      color: {
        green: (1 + greenBrightness) as 2,
        red: (1 + redBrightness) as 2,
      },
    });
  };

  if (!checkIfValidMatrixAddress(x, y)) {
    if (y === 0 && x === 8) {
      greenBrightness = (greenBrightness + 1) % 3;
    }
    if (y === 1 && x === 8) {
      redBrightness = (redBrightness + 1) % 3;
    }
    draw();
    return;
  }

  flipNeighbors(x, y);
  draw();
});

process.on("SIGINT", function () {
  try {
    input.close();
    resetLaunchpad(output);
    output.close();
  } catch (e) {}
  process.kill(0);
});
