export default (x: number, y: number) => {
  if (x < 0 || x > 8 || y < 0 || y > 7) {
    throw new Error("Invalid x/y coordinates (x 0-8, y 0-7 allowed)");
  }
  return Number.parseInt(`${y}${x}`, 16);
};
