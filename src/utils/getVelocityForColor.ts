import { FLAGS } from "./setPad";

export default (green: 0 | 1 | 2 | 3, red: 0 | 1 | 2 | 3, flags?: FLAGS) => {
  return 16 * green + red + (flags ?? FLAGS.NORMAL_USE);
};
