import { Output } from "easymidi";
import _setPad, { FLAGS } from "./setPad";

export default (
  output: Output,
  matrix: number[],
  {
    color = {
      red: 0,
      green: 3,
    },
    overwriteUnsetPads = true,
  }: {
    color?: {
      red: 0 | 1 | 2 | 3;
      green: 0 | 1 | 2 | 3;
    };
    overwriteUnsetPads?: boolean;
  } = {}
) => {
  const setPad = _setPad(output);

  if (matrix.length > 8) {
    throw new Error("Matrix is too big");
  }

  matrix.forEach((column, columnIndex) => {
    if (column > 255) {
      throw new Error(`matrix data on index ${columnIndex} is too big`);
    }
    const _columnData = column.toString(2);
    const columData = "00000000".substr(_columnData.length) + _columnData;
    const rows = columData.split("").reverse();

    rows.forEach((row, rowIndex) => {
      if (row === "1") {
        setPad(columnIndex, rowIndex, color.green, color.red, FLAGS.NORMAL_USE);
      } else if (overwriteUnsetPads) {
        setPad(columnIndex, rowIndex, 0, 0, FLAGS.NORMAL_USE);
      }
    });
  });
};
