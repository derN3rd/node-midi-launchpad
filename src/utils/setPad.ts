import { Output } from "easymidi";
import getPadAddress from "./getPadAddress";

export enum FLAGS {
  DOUBLE_BUFFERING = 0,
  LED_FLASH = 8,
  NORMAL_USE = 12,
}

export default (output: Output) => (
  x: number,
  y: number,
  green: 0 | 1 | 2 | 3,
  red: 0 | 1 | 2 | 3,
  flags: FLAGS
) => {
  const address = getPadAddress(x, y);
  const velocity = 16 * green + red + flags;
  output.send("noteon", {
    note: address,
    velocity,
    channel: 0,
  });
};
