import { Output } from "easymidi";
import _setPad from "./setPad";

export default (output: Output) => {
  const setPad = _setPad(output);

  setPad(0, 0, 0, 0, 12);
  setPad(1, 0, 0, 0, 12);
  setPad(2, 0, 1, 0, 12);
  setPad(3, 0, 1, 0, 12);
  setPad(4, 0, 2, 0, 12);
  setPad(5, 0, 2, 0, 12);
  setPad(6, 0, 3, 0, 12);
  setPad(7, 0, 3, 0, 12);
  setPad(0, 1, 0, 0, 12);
  setPad(1, 1, 0, 0, 12);
  setPad(2, 1, 1, 0, 12);
  setPad(3, 1, 1, 0, 12);
  setPad(4, 1, 2, 0, 12);
  setPad(5, 1, 2, 0, 12);
  setPad(6, 1, 3, 0, 12);
  setPad(7, 1, 3, 0, 12);

  setPad(0, 2, 0, 1, 12);
  setPad(1, 2, 0, 1, 12);
  setPad(2, 2, 1, 1, 12);
  setPad(3, 2, 1, 1, 12);
  setPad(4, 2, 2, 1, 12);
  setPad(5, 2, 2, 1, 12);
  setPad(6, 2, 3, 1, 12);
  setPad(7, 2, 3, 1, 12);
  setPad(0, 3, 0, 1, 12);
  setPad(1, 3, 0, 1, 12);
  setPad(2, 3, 1, 1, 12);
  setPad(3, 3, 1, 1, 12);
  setPad(4, 3, 2, 1, 12);
  setPad(5, 3, 2, 1, 12);
  setPad(6, 3, 3, 1, 12);
  setPad(7, 3, 3, 1, 12);

  setPad(0, 4, 0, 2, 12);
  setPad(1, 4, 0, 2, 12);
  setPad(2, 4, 1, 2, 12);
  setPad(3, 4, 1, 2, 12);
  setPad(4, 4, 2, 2, 12);
  setPad(5, 4, 2, 2, 12);
  setPad(6, 4, 3, 2, 12);
  setPad(7, 4, 3, 2, 12);
  setPad(0, 5, 0, 2, 12);
  setPad(1, 5, 0, 2, 12);
  setPad(2, 5, 1, 2, 12);
  setPad(3, 5, 1, 2, 12);
  setPad(4, 5, 2, 2, 12);
  setPad(5, 5, 2, 2, 12);
  setPad(6, 5, 3, 2, 12);
  setPad(7, 5, 3, 2, 12);

  setPad(0, 6, 0, 3, 12);
  setPad(1, 6, 0, 3, 12);
  setPad(2, 6, 1, 3, 12);
  setPad(3, 6, 1, 3, 12);
  setPad(4, 6, 2, 3, 12);
  setPad(5, 6, 2, 3, 12);
  setPad(6, 6, 3, 3, 12);
  setPad(7, 6, 3, 3, 12);
  setPad(0, 7, 0, 3, 12);
  setPad(1, 7, 0, 3, 12);
  setPad(2, 7, 1, 3, 12);
  setPad(3, 7, 1, 3, 12);
  setPad(4, 7, 2, 3, 12);
  setPad(5, 7, 2, 3, 12);
  setPad(6, 7, 3, 3, 12);
  setPad(7, 7, 3, 3, 12);
};
