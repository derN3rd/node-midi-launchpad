import { Output, Channel } from "easymidi";

export default (output: Output, brightness: 0 | 1 | 2) => {
  output.send("noteoff", {
    note: 0,
    velocity: 125 + brightness,
    channel: 48 as Channel, // to send signal to channel 176
  });
};
