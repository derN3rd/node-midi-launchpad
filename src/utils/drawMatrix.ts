import { Output } from "easymidi";
import _setPad, { FLAGS } from "./setPad";

export type Matrix = [
  [number, number, number, number, number, number, number, number],
  [number, number, number, number, number, number, number, number],
  [number, number, number, number, number, number, number, number],
  [number, number, number, number, number, number, number, number],
  [number, number, number, number, number, number, number, number],
  [number, number, number, number, number, number, number, number],
  [number, number, number, number, number, number, number, number],
  [number, number, number, number, number, number, number, number]
];
/**
 * @example ```
  const heart = [
    [0,1,1,0,0,1,1,0],
    [1,0,0,1,1,0,0,1],
    [1,0,0,0,0,0,0,1],
    [1,0,0,0,0,0,0,1],
    [0,1,0,0,0,0,1,0],
    [0,0,1,0,0,1,0,0],
    [0,0,0,1,1,0,0,0],
    [0,0,0,0,0,0,0,0]
  ];
  drawMatrix(output, heart);
 ```
 */
export default (
  output: Output,
  matrix: Matrix,
  {
    color = {
      red: 0,
      green: 3,
    },
    overwriteUnsetPads = true,
  }: {
    color?: {
      red: 0 | 1 | 2 | 3;
      green: 0 | 1 | 2 | 3;
    };
    overwriteUnsetPads?: boolean;
  } = {}
) => {
  const setPad = _setPad(output);

  matrix.forEach((row, rowIndex) => {
    row.forEach((column, columnIndex) => {
      if (column === 1) {
        setPad(columnIndex, rowIndex, color.green, color.red, FLAGS.NORMAL_USE);
      } else if (overwriteUnsetPads) {
        setPad(columnIndex, rowIndex, 0, 0, FLAGS.NORMAL_USE);
      }
    });
  });
};
