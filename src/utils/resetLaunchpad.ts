import { Channel, Output } from "easymidi";

export default (output: Output) =>
  output.send("noteoff", {
    note: 0,
    velocity: 0,
    channel: 48 as Channel, // to send signal to channel 176 which will reset the launchpad
  });
