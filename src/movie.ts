import easymidi from "easymidi";
import drawVerticalMatrix from "./utils/drawVerticalMatrix";
import resetLaunchpad from "./utils/resetLaunchpad";
import _setPad from "./utils/setPad";

// all data about launchpad midi from: http://leemans.ch/latex/doc_launchpad-programmers-reference.pdf

const allInputs = easymidi.getInputs();
const launchpadMidiInputName = allInputs.find((name) =>
  name.includes("Launchpad")
);
const allOutputs = easymidi.getOutputs();
const launchpadMidiOutputName = allOutputs.find((name) =>
  name.includes("Launchpad")
);

if (!launchpadMidiInputName || !launchpadMidiOutputName) {
  console.error(
    "Couldn't find Launchpad MIDI device. Found only\nInputs:",
    allInputs.join(", "),
    "\nOutputs:",
    allOutputs.join(", ")
  );
  process.exit(1);
}
const input = new easymidi.Input(launchpadMidiInputName);
const output = new easymidi.Output(launchpadMidiOutputName);

const setPad = _setPad(output);

resetLaunchpad(output);

const frames = [
  [
    0b11111111,
    0b10000001,
    0b10000001,
    0b10000001,
    0b10000001,
    0b10000001,
    0b10000001,
    0b11111111,
  ],
  [
    0b00000000,
    0b01111110,
    0b01000010,
    0b01000010,
    0b01000010,
    0b01000010,
    0b01111110,
    0b00000000,
  ],
  [
    0b00000000,
    0b00000000,
    0b00111100,
    0b00100100,
    0b00100100,
    0b00111100,
    0b00000000,
    0b00000000,
  ],
  [
    0b00000000,
    0b00000000,
    0b00000000,
    0b00011000,
    0b00011000,
    0b00000000,
    0b00000000,
    0b00000000,
  ],
  [
    0b00000000,
    0b00000000,
    0b00111100,
    0b00100100,
    0b00100100,
    0b00111100,
    0b00000000,
    0b00000000,
  ],
  [
    0b00000000,
    0b01111110,
    0b01000010,
    0b01000010,
    0b01000010,
    0b01000010,
    0b01111110,
    0b00000000,
  ],
];

let counter = 0;
const interval = setInterval(() => {
  if (counter === frames.length - 1) {
    counter = 0;
  } else {
    counter++;
  }

  drawVerticalMatrix(output, frames[counter], {
    color: {
      green: 3,
      red: 1,
    },
  });
}, 100);

process.on("SIGINT", function () {
  try {
    clearInterval(interval);
    input.close();
    resetLaunchpad(output);
    output.close();
  } catch (e) {}
  process.kill(0);
});
