import easymidi from "easymidi";
import getVelocityForColor from "./utils/getVelocityForColor";
import resetLaunchpad from "./utils/resetLaunchpad";
import _setPad, { FLAGS } from "./utils/setPad";

// all data about launchpad midi from: http://leemans.ch/latex/doc_launchpad-programmers-reference.pdf

const allInputs = easymidi.getInputs();
const launchpadMidiInputName = allInputs.find((name) =>
  name.includes("Launchpad")
);
const allOutputs = easymidi.getOutputs();
const launchpadMidiOutputName = allOutputs.find((name) =>
  name.includes("Launchpad")
);

if (!launchpadMidiInputName || !launchpadMidiOutputName) {
  console.error(
    "Couldn't find Launchpad MIDI device. Found only\nInputs:",
    allInputs.join(", "),
    "\nOutputs:",
    allOutputs.join(", ")
  );
  process.exit(1);
}
const input = new easymidi.Input(launchpadMidiInputName);
const output = new easymidi.Output(launchpadMidiOutputName);

const setPad = _setPad(output);

resetLaunchpad(output);

let brightness = 1 as 0 | 1 | 2 | 3;

enum SELECTED_COLOR {
  GREEN,
  RED,
  YELLOW,
}

let selectedColor: SELECTED_COLOR = SELECTED_COLOR.GREEN;

setPad(8, 0, 1, 1, FLAGS.NORMAL_USE);
setPad(8, 1, 1, 1, FLAGS.NORMAL_USE);

const updateColors = () => {
  const greenAddress = [8, 3] as [number, number];
  const redAddress = [8, 4] as [number, number];
  const yellowAddress = [8, 5] as [number, number];

  setPad(...greenAddress, brightness, 0, FLAGS.NORMAL_USE);
  setPad(...redAddress, 0, brightness, FLAGS.NORMAL_USE);
  setPad(...yellowAddress, brightness, brightness, FLAGS.NORMAL_USE);
};

updateColors();

input.on("noteon", function (msg) {
  if (msg.velocity === 0) return;
  //console.log(msg);

  if (msg.note === 120) {
    resetLaunchpad(output);
    return;
  }
  if (msg.note === 8) {
    brightness = Math.max(0, brightness - 1) as 0 | 1 | 2 | 3;
    updateColors();
    return;
  }
  if (msg.note === 24) {
    brightness = Math.min(3, brightness + 1) as 0 | 1 | 2 | 3;
    updateColors();
    return;
  }

  if (msg.note === 56) {
    selectedColor = SELECTED_COLOR.GREEN;
    return;
  }
  if (msg.note === 72) {
    selectedColor = SELECTED_COLOR.RED;
    return;
  }
  if (msg.note === 88) {
    selectedColor = SELECTED_COLOR.YELLOW;
    return;
  }

  let velocity = 0;
  switch (selectedColor) {
    case SELECTED_COLOR.GREEN:
      velocity = getVelocityForColor(brightness, 0);
      break;
    case SELECTED_COLOR.RED:
      velocity = getVelocityForColor(0, brightness);
      break;
    case SELECTED_COLOR.YELLOW:
      velocity = getVelocityForColor(brightness, brightness);
      break;
  }

  output.send("noteon", {
    note: msg.note,
    velocity,
    channel: 0,
  });
});

process.on("SIGINT", function () {
  try {
    input.close();
    resetLaunchpad(output);
    output.close();
  } catch (e) {}
  process.kill(0);
});
